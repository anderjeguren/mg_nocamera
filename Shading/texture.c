
#include <stdlib.h>
#include <stdio.h>
#include "texture.h"
#include "tools.h"
#include "image.h"

// Forward declarations
static void send_texparams_gl(Texture *thisTexture);
static void send_image_gl(Texture *thisTexture);

static const char *TT_string(int type);
static char *PT_string(GLenum e);
static void set_texture_RT(Texture *thisTexture, int height, int width,
						   int components, GLuint internalFormat, GLuint format);

// Create textures

Texture *CreateVoidTexture(char *img) {

	Texture *newTexture;

	newTexture = malloc(sizeof(*newTexture));

	newTexture->type = MG_TEX_TEX;
	newTexture->target = GL_TEXTURE_2D;

	newTexture->mipmap = 0;
	newTexture->magFilter = GL_LINEAR;
	newTexture->minFilter = GL_LINEAR;

	newTexture->wrapS = GL_REPEAT;
	newTexture->wrapT = GL_REPEAT;

	//Allocates a texture name
	glGenTextures( 1, &newTexture->textureId );

	newTexture->height = 0;
	newTexture->width = 0;
	newTexture->size = 0;
	newTexture->components = 3;
	newTexture->format = GL_RGB;

	newTexture->img = 0;

	return newTexture;
}

void DestroyTexture (Texture **theTexture) {

	Texture *thisTexture = *theTexture;
	/* DecrBitmapRefCount(thisTexture); */
	if (!thisTexture) return;
	glDeleteTextures(1, &thisTexture->textureId);
	thisTexture->img = NULL;
	free(thisTexture);
	*theTexture = NULL;
}


void SetImageTexture(Texture *thisTexture,
					 char *FileName) {

	Img *img;

	if(thisTexture->img) {
		//remove image data from openGL
		glDeleteTextures(1, &thisTexture->textureId);
		//Allocates a texture name
		glGenTextures( 1, &thisTexture->textureId );
	}

	img = SceneRegisterImage(FileName);
	thisTexture->img = img;
	thisTexture->size = img->size;
	thisTexture->height = img->height;
	thisTexture->width = img->width;
	thisTexture->format = GL_RGB;
	thisTexture->components = 3;

	// Filters
	if (thisTexture->mipmap) {
		// Filters
		thisTexture->magFilter = GL_LINEAR;
		thisTexture->minFilter = GL_NEAREST_MIPMAP_NEAREST;
	} else {
		thisTexture->magFilter = GL_LINEAR;
		thisTexture->minFilter = GL_LINEAR;
	}
	// Send image into openGL texture
	send_image_gl(thisTexture);
	// Send texture data to OpenGL (initialize OpenGL texture)
	send_texparams_gl(thisTexture);
}

void SetBumpTexture(Texture *thisTexture,
					char *FileName) {

	Img *img;

	thisTexture->type = MG_TEX_BUMPMAP;
	thisTexture->mipmap = 0; // no mipmap for bump textures
	SetImageTexture(thisTexture, FileName);
}

void SetProjTexture(Texture *thisTexture,
					char *FileName) {

	thisTexture->type = MG_TEX_PROJ;
	thisTexture->mipmap = 0; // no mipmap for projective textures
	SetImageTexture(thisTexture, FileName);
	thisTexture->magFilter = GL_LINEAR;
	thisTexture->minFilter = GL_LINEAR;
	thisTexture->wrapS = GL_CLAMP_TO_BORDER;
	thisTexture->wrapT = GL_CLAMP_TO_BORDER;
	send_texparams_gl(thisTexture);
}

void SetCubeMapTexture(Texture *thisTexture,
					   char *xpos, char *xneg,
					   char *ypos, char *yneg,
					   char *zpos, char *zneg) {

	thisTexture->type = MG_TEX_CUBEMAP;
	thisTexture->target = GL_TEXTURE_CUBE_MAP;

	Img *img;
	int i = 0;
	GLuint targets[6] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
	char *names[6] = { xpos, xneg, ypos, yneg, zpos, zneg };


	/* // Typical cube map settings */
	thisTexture->magFilter = GL_LINEAR;
	thisTexture->minFilter = GL_LINEAR;
	thisTexture->wrapS = GL_CLAMP_TO_EDGE;
	thisTexture->wrapT = GL_CLAMP_TO_EDGE;
	thisTexture->wrapR = GL_CLAMP_TO_EDGE;

	BindGLTexture(thisTexture);
	for(i = 0; i < 6; i++) {
		img = SceneRegisterImage(names[i]);
		glTexImage2D(targets[i], 0, 3,
					 img->width, img->height,
					 0, GL_RGB, GL_UNSIGNED_BYTE, img->data);
	}

	/* // Set OpenGL texture wrap */
	/* glTexParameteri( thisTexture->target, GL_TEXTURE_WRAP_S, thisTexture->wrapS); */
	/* glTexParameteri( thisTexture->target, GL_TEXTURE_WRAP_T, thisTexture->wrapT); */
	/* glTexParameterf(thisTexture->target, GL_TEXTURE_WRAP_R, thisTexture->wrapR); */

	/* // Set OpenGL texture filters */
	/* glTexParameteri( thisTexture->target, GL_TEXTURE_MAG_FILTER, thisTexture->magFilter); */
	/* glTexParameteri( thisTexture->target, GL_TEXTURE_MIN_FILTER, thisTexture->minFilter); */

	//UnbindGLTexture(thisTexture);
	send_texparams_gl(thisTexture);
}

// SetDepthTexture: Set texture so it can store depth values (for shadow maps, etc)

void SetDepthMapTexture(Texture *thisTexture, int height, int width) {

	set_texture_RT(thisTexture, height, width, 1, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT);
	thisTexture->type = MG_TEX_DEPTH;
	//BindGLTexture(thisTexture);
	/* glTexParameteri(thisTexture->target, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY); */
	/* glTexParameteri(thisTexture->target, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE); */
	/* glTexParameteri(thisTexture->target, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL); */
	// Restore old texture
	//UnbindGLTexture(thisTexture);
}

void SetColorMapTexture(Texture *thisTexture, int height, int width) {

	set_texture_RT(thisTexture, height, width, 3, GL_RGB, GL_RGB);
	thisTexture->type = MG_TEX_COLOR;
}


void SetWhiteTexture(Texture *thisTexture) {

	static unsigned char white[3] = {255, 255, 255};

	if(thisTexture->img) {
		//remove image data from openGL
		glDeleteTextures(1, &thisTexture->textureId);
		//Allocates a texture name
		glGenTextures( 1, &thisTexture->textureId );
	}
	thisTexture->size = 3;
	thisTexture->height = 1;
	thisTexture->width = 1;
	thisTexture->format = GL_RGB;
	thisTexture->components = 3;

	thisTexture->mipmap = 0;
	thisTexture->magFilter = GL_NEAREST;
	thisTexture->minFilter = GL_NEAREST;
	thisTexture->wrapS = GL_REPEAT;
	thisTexture->wrapT = GL_REPEAT;

	// Set our texture as active
	BindGLTexture(thisTexture);
	glTexImage2D( thisTexture->target, 0, thisTexture->components, thisTexture->width,
				  thisTexture->height, 0, thisTexture->format, GL_UNSIGNED_BYTE,
				  &white[0]);
	// Send texture data to OpenGL (initialize OpenGL texture)
	send_texparams_gl(thisTexture);
}

// BindGLTexture
//
// Set texture as "active" in the zero texture unit. Subsequent texture
// operations are done on this texture.

void BindGLTexture(Texture *thisTexture) {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture( thisTexture->target, thisTexture->textureId );
}

// Unbind texture
void UnbindGLTexture(Texture *thisTexture) {
	glBindTexture( thisTexture->target, 0 );
}

void BindGLTextureTunit(Texture *thisTexture, int location) {
	// Set texture unit index
	glActiveTexture(GL_TEXTURE0 + location);
	glBindTexture( thisTexture->target, thisTexture->textureId );
}

void UnbindGLTextureTunit(Texture *thisTexture, int location) {
	// Set texture unit index
	glActiveTexture(GL_TEXTURE0 + location);
	glBindTexture( thisTexture->target, 0 );
}

/* Set/get functions */


int SetMipmapTexture(Texture *thisTexture, int mipmap) {
	int old = thisTexture->mipmap;
	thisTexture->mipmap = mipmap;
	return old;
}

int GetMipmapTexture(Texture *thisTexture) {
	return thisTexture->mipmap;
}

int GetTypeTexture(Texture *theTexture) {
	return theTexture->type;
}

// Wrap

void SetTextureWrapST(Texture *thisTexture, GLenum wrapS, GLenum wrapT) {

	thisTexture->wrapS = wrapS;
	thisTexture->wrapT = wrapT;
	send_texparams_gl(thisTexture);
}

void SetTextureWrapS(Texture *thisTexture, GLenum wrapS) {

	thisTexture->wrapS = wrapS;
	send_texparams_gl(thisTexture);
}

void SetTextureWrapT(Texture *thisTexture, GLenum wrapT) {
	thisTexture->wrapT = wrapT;
	send_texparams_gl(thisTexture);
}

static int cycleTexEnum(GLenum *fil, int m,
						GLenum theFil) {
	int fil_i = 0;
	for(fil_i = 0; fil_i != m; ++fil_i) {
		if(fil[fil_i] == theFil)
			break;
	}
	fil_i++;
	return (fil_i % m);
}

void CycleTextureWrapS(Texture *thisTexture) {

	static GLenum wrap_fil[] = { GL_CLAMP, GL_REPEAT };
	//static char *wrap_fil_str[] = { "GL_CLAMP", "GL_REPEAT" };

	int idx = cycleTexEnum(wrap_fil, 2, thisTexture->wrapS);
	SetTextureWrapS(thisTexture, wrap_fil[idx]);
	//printf("%s\n", wrap_fil_str[idx]);
}

void CycleTextureWrapT(Texture *thisTexture) {

	static GLenum wrap_fil[] = { GL_CLAMP, GL_REPEAT };
	//static char *wrap_fil_str[] = { "GL_CLAMP", "GL_REPEAT" };

	int idx = cycleTexEnum(wrap_fil, 2, thisTexture->wrapT);
	SetTextureWrapT(thisTexture, wrap_fil[idx]);
	//printf("%s\n", wrap_fil_str[idx]);
}

// Filters

void SetTextureFilters(Texture *thisTexture, GLenum minFilter, GLenum magFilter) {
	thisTexture->magFilter = magFilter;
	thisTexture->minFilter = minFilter;
	send_texparams_gl(thisTexture);
}

void SetTextureMinFilter(Texture *thisTexture, GLenum minFilter) {
	thisTexture->minFilter = minFilter;
	send_texparams_gl(thisTexture);
}


void CycleTextureMagFilter(Texture *thisTexture) {

	static GLenum mag_fil[] = { GL_NEAREST, GL_LINEAR };

	int idx = cycleTexEnum(mag_fil, 2, thisTexture->magFilter);
	SetTextureMagFilter(thisTexture, mag_fil[idx]);
}

void CycleTextureMinFilter(Texture *thisTexture) {

	static GLenum min_fil[] = { GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST,
								GL_LINEAR_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR,
								GL_LINEAR_MIPMAP_LINEAR };

	int idx = cycleTexEnum(min_fil, 6, thisTexture->minFilter);
	SetTextureMinFilter(thisTexture, min_fil[idx]);
	//printf("%s\n", min_fil_str[idx]);
}

void SetTextureMagFilter(Texture *thisTexture, GLenum magFilter) {
	thisTexture->magFilter = magFilter;
	send_texparams_gl(thisTexture);
}

/**
 * Send texture parameters to OpenGL
 *
 * hints:
 *        - use glTexParameteri for setting filters/wrap
 *        - use glTexImage2D or gluBuild2DMipmaps for loading image into graphic card
 *          -- internalFormat: 3
 *          -- format:         GL_RGB
 *          -- border:         0
 *          -- type:           GL_UNSIGNED_BYTE
 *
 */

static void send_texparams_gl(Texture *thisTexture) {

	// Set our texture as active
	BindGLTexture(thisTexture);

	// Set OpenGL texture wrap
	glTexParameteri( thisTexture->target, GL_TEXTURE_WRAP_S, thisTexture->wrapS);
	glTexParameteri( thisTexture->target, GL_TEXTURE_WRAP_T, thisTexture->wrapT);
	if (thisTexture->type == MG_TEX_CUBEMAP) {
		glTexParameterf(thisTexture->target, GL_TEXTURE_WRAP_R,
						thisTexture->wrapR);
	}

	// Set OpenGL texture filters
	glTexParameteri( thisTexture->target, GL_TEXTURE_MAG_FILTER, thisTexture->magFilter);
	glTexParameteri( thisTexture->target, GL_TEXTURE_MIN_FILTER, thisTexture->minFilter);

	UnbindGLTexture(thisTexture);
}

/**
 * Send image to OpenGL
 *
 * hints:
 *        - use glTexImage2D or gluBuild2DMipmaps for loading image into graphic card
 *          -- internalFormat: 3
 *          -- format:         GL_RGB
 *          -- border:         0
 *          -- type:           GL_UNSIGNED_BYTE
 *
 */


static void send_image_gl(Texture *thisTexture) {

	Img *img = thisTexture->img;

	if (!img) return;

	// Set our texture as active
	BindGLTexture(thisTexture);

	if (img->height && img->width) {
		// Load image to OpenGL texture
		if (thisTexture->mipmap) {
			gluBuild2DMipmaps(thisTexture->target, thisTexture->components, img->width, img->height,
							  thisTexture->format, GL_UNSIGNED_BYTE, img->data);
		} else {
			glTexImage2D( thisTexture->target, 0, thisTexture->components, img->width,
						  img->height, 0, thisTexture->format, GL_UNSIGNED_BYTE,
						  img->data);
		}
	}
	UnbindGLTexture(thisTexture);
}

// set Render Target (RT) texture:
//  - properly set filters and anti-aliasing setings
//  - reclaim memory in OpenGL for storing the texture

static void set_texture_RT(Texture *thisTexture, int height, int width,
						   int components, GLuint internalFormat, GLuint format) {


	thisTexture->mipmap = 0; // No mipmap for target textures
	if (thisTexture->img) {
		// The texture has a previous image. Delete OpenGL's texture object.
		glDeleteTextures(1, &thisTexture->textureId);
		thisTexture->img = 0;
		// Allocates new texture object
		glGenTextures( 1, &thisTexture->textureId );
	}
	thisTexture->height = height;
	thisTexture->components = components;
	thisTexture->format = internalFormat;

	thisTexture->width = width;
	thisTexture->size = components * height * width;
	if (!thisTexture->size) {
		fprintf(stderr, "[E] texture with zero size\n");
		exit(1);
	}

	thisTexture->magFilter = GL_NEAREST;
	thisTexture->minFilter = GL_NEAREST;
	thisTexture->wrapS = GL_CLAMP;
	thisTexture->wrapT = GL_CLAMP;

	BindGLTexture(thisTexture);
	send_texparams_gl(thisTexture);
	// Reclaim OpenGL space for image data
	BindGLTexture(thisTexture);
	glTexImage2D( thisTexture->target, 0, internalFormat, thisTexture->width,
				  thisTexture->height, 0, format, GL_UNSIGNED_BYTE,
				  0);
	UnbindGLTexture(thisTexture);
}

static const char *TT_string(int e) {

	static int N[] = {MG_TEX_TEX, MG_TEX_DEPTH, MG_TEX_COLOR, MG_TEX_CUBEMAP, MG_TEX_BUMPMAP, MG_TEX_PROJ};
	static char *T[]  = {"MG_TEX_TEX", "MG_TEX_DEPTH", "MG_TEX_COLOR", "MG_TEX_CUBEMAP", "MG_TEX_BUMPMAP", "MG_TEX_PROJ"};

	int i;
	int m;

	m = sizeof(T) / sizeof(char *);
	for(i=0; i < m; i++) {
		if (e == N[i]) return T[i];
	}
	return "<unknown>";
}


static char *PT_string(GLenum e) {

	static GLenum N[] = {GL_TEXTURE_2D, GL_TEXTURE_CUBE_MAP, GL_CLAMP, GL_CLAMP_TO_BORDER, GL_REPEAT, GL_LINEAR, GL_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_RGB, GL_RGB8, GL_DEPTH_COMPONENT};
	static char *T[]  = {"GL_TEXTURE_2D", "GL_TEXTURE_CUBE_MAP", "GL_CLAMP", "GL_CLAMP_TO_BORDER", "GL_REPEAT", "GL_LINEAR", "GL_NEAREST", "GL_NEAREST_MIPMAP_LINEAR", "GL_NEAREST_MIPMAP_NEAREST", "GL_LINEAR_MIPMAP_LINEAR", "GL_LINEAR_MIPMAP_NEAREST", "GL_RGB", "GL_RGB8", "GL_DEPTH_COMPONENT"};
	int i;
	int m;

	m = sizeof(T) / sizeof(char *);
	for(i=0; i < m; i++) {
		if (e == N[i]) return T[i];
	}
	return "<unknown>";
}

void PrintTexture (Texture *thisTexture) {

	Img *img;
	//printf("File Name: %s\n", thisTexture->fileName);
	printf("Texture id:%d\tType: %s\ttarget:%s\n", thisTexture->textureId, TT_string(thisTexture->type), PT_string(thisTexture->target));
	img = thisTexture->img;
	if (!img) {
		printf ("No jpeg image\n");
	} else {
		printf("JPG image: %s\tResolution: %d x %d\tSize: %lu\tMipmap: %d\tData: %p\n", img->fileName, thisTexture->width, thisTexture->height,
			   thisTexture->size, thisTexture->mipmap, img->data);

	}
	if (!img && thisTexture->size) {
		printf("Generated image\tResolution: %d x %d \t Size: %lu\tMipmap: %d\n", thisTexture->width, thisTexture->height,
			   thisTexture->size, thisTexture->mipmap);
	}
	printf("Format: %s\tComponents:%d\n", PT_string(thisTexture->format), thisTexture->components);
	printf("Filters (min,mag): %s, %s\t Wrap (s,t): %s,%s\n\n",
			 PT_string(thisTexture->minFilter), PT_string(thisTexture->magFilter),
			 PT_string(thisTexture->wrapS), PT_string(thisTexture->wrapT));
}

