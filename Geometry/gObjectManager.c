#include "gObjectManager.h"
#include "hash.h"
#include <string.h>
#include <stddef.h>

static hash *gObj_hash = NULL;

// Create a key given a directory and filename

static char *create_gkey(char *dir, char *name) {

	static char MG_SC_KEY[2048];

	size_t lgh;
	size_t ldir;

	ldir = strlen(dir);
	lgh = ldir + strlen(name) + 2;
	if (lgh > 2047) {
		fprintf(stderr,"[E] gObject name too long!\n%s/%s\n", dir, name);
		exit(1);
	}
	strcpy(&MG_SC_KEY[0], dir);
	if (ldir && dir[ldir - 1] == '/') ldir--;
	MG_SC_KEY[ldir] = '/';
	strcpy(&MG_SC_KEY[ldir + 1], name);
	return MG_SC_KEY;
}

void InitGObjectManager() {
	gObj_hash = CreateVoidHash();
}

void DestroyGObjectManager() {

	GObject *gObj;

	if (gObj_hash == NULL) return;
	gObj = StartLoopHash( gObj_hash );
	while(gObj) {
		DestroyGObject(&gObj);
		gObj = GetNextHash( gObj_hash );
	}
	DestroyHash(&gObj_hash);
	gObj_hash = NULL;
}

// Geometry objects

GObject *SceneFindGObject(char *dir, char *fname) {

	char *key;

	key = create_gkey(dir, fname);
	return FindHashElement(gObj_hash, key);
}

GObject *SceneFindNameGObject(char *name) {

	return FindHashElement(gObj_hash, name);
}

GObject *SceneRegisterGObject(char *dir, char *filename) {

	char *key;
	GObject *theGobj;

	key = create_gkey(dir, filename);
	theGobj = FindHashElement(gObj_hash, key);
	if (!theGobj) {
		theGobj = InsertHashElement(gObj_hash, key, CreateFromWavefront(dir, filename));
	} else {
		fprintf(stderr, "[W] duplicate geometric object %s/%s. Skipping.\n", dir, filename);
	}
	return theGobj;
}

GObject *SceneRegisterVoidGObject(char *dir, char *filename) {
	char *key;
	GObject *theGobj;

	key = create_gkey(dir, filename);
	theGobj = FindHashElement(gObj_hash, key);
	if (!theGobj) {
		theGobj = InsertHashElement(gObj_hash, key, CreateVoidGObject());
	} else {
		fprintf(stderr, "[W] duplicate geometric object %s/%s. Skipping.\n", dir, filename);
	}
	return theGobj;
}

void PrintRegisteredGobjs() {

	char *key;

	key = StartKeyLoopHash(gObj_hash);
	while(key) {
		printf ("Gobj: %s\n", key);
		key = GetNextKeyHash(gObj_hash);
	}
}
