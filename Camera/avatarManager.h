#ifndef AVATARMANAGER_H
#define AVATARMANAGER_H

#include "avatar.h"

void InitAvatarManager();
void DestroyAvatarManager();

/**
 * Get a registered avatar
 *
 *
 * @param avatarName: The name of the avatar
 * @return the avatar or NULL if not found.
 */

Avatar *SceneFindAvatar(char *avatarName);

/**
 * Register a new avatar
 *
 * If avatarName is new, create new avatar and register it.
 * If avatarName already exists, return the associated avatar.
 *
 * @param avatarName: The name of the avatar
 * @return the new or existing avatar
 */

Avatar *SceneRegisterAvatar(char *avatarName, Camera *theCamera, int radius);

/**
 * Destroy a registered avatar
 *
 * @param texName: The name of the avatar
 */

void SceneDestroyAvatar(char *avatarName);

void PrintRegisteredAvatars();

#endif // AVATARMANAGER_H
