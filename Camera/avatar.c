#include "tools.h"
#include "scene.h"
#include "avatar.h"

void DestroyAvatar ( Avatar **theAvatar ) {

	Avatar  *thisAvatar = *theAvatar;
	if( ! thisAvatar ) return;

	DestroyBSphere(&thisAvatar->bsph);
	if (thisAvatar->name)
		free(thisAvatar->name);
	free( thisAvatar );

	*theAvatar = NULL;
}

Avatar *CreateAvatar( char *name, Camera *theCamera, float radius) {

	Avatar  *newAvatar;
	BSphere *newBSphere;
	float x, y, z;

	newAvatar = malloc( sizeof(*newAvatar) * 1 );
	newAvatar->name = strdup(name);
	GetCameraPosition(theCamera, &x, &y, &z);
	newBSphere = CreateBSphere( x, y, z, radius);

	newAvatar->cam          = theCamera;
	newAvatar->bsph         = newBSphere;
	newAvatar->walk         = 0;

	return newAvatar;
}

int WalkOrFlyAvatar(Avatar *theAvatar, int walkOrFly) {
	int walk = theAvatar->walk;
	theAvatar->walk = walkOrFly;
	return walk;
}

// TODO:
//
// AdvanceAvatar: advance avatar 'step' units if possible.

int AdvanceAvatar(Avatar *theAvatar, float step) {

	Camera *thisCamera = theAvatar->cam;
	BSphere *bsph = theAvatar->bsph;
	float newX, newY, newZ;
	Node *rootNode;

	rootNode = RootNodeScene();

	/*Eguneratu besfera*/
	bsph->sphereX = thisCamera->Ex + thisCamera->Dx * step ;
	bsph->sphereY = thisCamera->Ey + thisCamera->Dy * step ;
	bsph->sphereZ = thisCamera->Ez + thisCamera->Dz * step ;

	if(!CollisionBSphereNode(rootNode, bsph)){
		if(theAvatar->walk == 0) FlyCamera(thisCamera, step);

		else if(theAvatar->walk == 1) WalkCamera(thisCamera, step);
	}


	return 1;
}

void LeftRightAvatar(Avatar *theAvatar, float angle) {
	if (theAvatar->walk)
		ViewYWorldCamera(theAvatar->cam, angle);
	else
		YawCamera(theAvatar->cam, angle);
}

void UpDownAvatar(Avatar *theAvatar, float angle) {
	PitchCamera(theAvatar->cam, angle);
}

void PrintAvatar(Avatar *theAvatar) {
}
