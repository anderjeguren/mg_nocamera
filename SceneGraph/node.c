#include <stdlib.h>
#include <string.h>
#include "hash.h"
#include "node.h"
#include "intersect.h"
#include "bboxGL.h"
#include "scene.h"


// manager functions

static hash *node_hash = NULL;

void InitNodeManager() {
	node_hash = CreateVoidHash();
}

void DestroyNodeManager() {
	Node *oneNode;

	if (node_hash == NULL) return;
	oneNode = StartLoopHash( node_hash );
	while(oneNode) {
		DestroyNode(&oneNode);
		oneNode = GetNextHash( node_hash );
	}
	DestroyHash(&node_hash);
}

// forward declarations of auxiliary functions

static Node* CloneParentNode(Node *sNode, Node *theParent);
static void nodeUpdateBB (Node *thisNode);
static void nodePropagateBBRoot(Node * thisNode);
static void nodeUpdateGS( Node *thisNode);

Node *SceneFindNode(char *name) {
	return FindHashElement(node_hash, name);
}

Node *CreateVoidNode() {

	Node * thisNode;

	thisNode = malloc(sizeof(*thisNode) * 1);
    thisNode->name = NULL;
	thisNode->gObject = NULL;
	thisNode->theLight = NULL;
	thisNode->theShader = NULL;
	thisNode->nodeChilds = CreateVoidList();
	thisNode->placement = CreateTrfm3D();
	thisNode->placement_WC = CreateTrfm3D();
	thisNode->container_WC = CreateVoidBBox();
	thisNode->parent = NULL;
	thisNode->isCulled = 0;
	thisNode->drawBBox = 0;

	return thisNode;
}

Node *CreateNode(char *nodeName) {

	Node *theNode = NULL;

	theNode = FindHashElement(node_hash, nodeName);
	if (theNode) {
		fprintf(stderr, "[E] CreateNode: duplicate node %s\n.", nodeName);
        exit(1);
	}
    theNode = CreateVoidNode();
    theNode->name = strdup(nodeName);
	// New node
	return InsertHashElement(node_hash, nodeName, theNode);
}

void DestroyNode(Node **theNode) {

	Node * oneNode;
	Node  *thisNode = *theNode;

	if( ! thisNode ) return;

	// destroy Node list
	oneNode = StartLoop(thisNode->nodeChilds);
	while(oneNode) {
		DestroyNode(&oneNode); // Recursive call
		oneNode = GetNext(thisNode->nodeChilds);
	}
	DestroyList(&thisNode->nodeChilds);

    if (thisNode->name) free(thisNode->name);
	DestroyBBox(&thisNode->container_WC);
	DestroyTrfm3D(&thisNode->placement);
	DestroyTrfm3D(&thisNode->placement_WC);
	thisNode->gObject = NULL;
	thisNode->theLight = NULL;
	thisNode->theShader = NULL;
	thisNode->parent = NULL;

	free( thisNode );

	*theNode = NULL;
}



Node * CycleChildNode(Node *theNode, size_t idx) {

	size_t   i;
	size_t   m;
	Node     *cur = NULL;

	if( theNode == NULL ) return NULL;

	m = idx % ElementsInList(theNode->nodeChilds);
	for( i = 0, cur = StartLoop(theNode->nodeChilds);
		 cur && i < m;
		 ++i, cur = GetNext(theNode->nodeChilds));
	return cur;
}

Node *NextSiblingNode(Node *theNode) {
	Node *theParent;
	Node *theSibling = NULL;

	if (!theNode) return NULL;
	theParent = theNode->parent;
	if (!theParent) return theNode;
	theSibling = StartLoop(theParent->nodeChilds);
	while(theSibling && theSibling != theNode) {
		theSibling = GetNext(theParent->nodeChilds);
	}
	theSibling = GetNext(theParent->nodeChilds);
	if (!theSibling) return StartLoop(theParent->nodeChilds);
	return theSibling;
}


Node *FirstChildNode(Node *theNode) {

	Node *theChild = NULL;

	if (!theNode) return NULL;
	theChild = StartLoop(theNode->nodeChilds);
	if (!theChild) return theNode;
	return theChild;
}

Node *ParentNode(Node *theNode) {
	if (!theNode) return NULL;
	if (!theNode->parent) return theNode;
	return theNode->parent;
}

static char *name_clone(char *base) {

    static char MG_SC_BUFF[2048];
    Node *aux;

    int i;
    for(i = 1; i < 1000; i++) {
        sprintf(MG_SC_BUFF, "%s#%d", base, i);
        aux = FindHashElement(node_hash, MG_SC_BUFF);
        if(!aux) return MG_SC_BUFF;
    }
    fprintf(stderr, "[E] Node: too many clones of %s\n.", base);
    exit(1);
}

static Node* CloneParentNode(Node *sNode, Node *theParent) {

	Node *newNode;
	Node *oneNode;

    char *newName = name_clone(sNode->name);
    if (!newName) {
        fprintf(stderr, "[E] CloneParentNode: can't clone node with no name\n.");
        exit(1);
    }
	newNode = CreateNode(newName);
	newNode->gObject = sNode->gObject;
	newNode->theLight = sNode->theLight;
	newNode->theShader = sNode->theShader;
	SetCopyTrfm3D(newNode->placement, sNode->placement);
	newNode->parent = theParent;

	oneNode = StartLoop(sNode->nodeChilds);
	while(oneNode) {
		AddLast(newNode->nodeChilds,
				CloneParentNode(oneNode, newNode)); // recursive call
		oneNode = GetNext(sNode->nodeChilds);
	}
	return newNode;
}


Node *CloneNode(Node *sNode) {

	if (sNode == NULL) return NULL;
	return CloneParentNode(sNode, NULL);

}

// auxiliary function
//
// given a node:
// - update the BBox of the node (nodeUpdateBB)
// - Propagate BBox to parent until root is reached
//
// - Preconditions:
//    - the BBox of thisNode's children are up-to-date.
//    - placement_WC of node and parents are up-to-date

static void nodePropagateBBRoot(Node * thisNode) {

	Node *oneNode;
	for (oneNode = thisNode; oneNode; oneNode = oneNode->parent)
		nodeUpdateBB(oneNode);
}

// auxiliary function
//
// given a node, update its BBox to World Coordinates so that it includes:
//  - the BBox of the geometricObject it contains (if any)
//  - the BBox-es of the children
//
// Note: Bounding box is always in world coordinates
//
// Precontitions:
//
//     * thisNode->placement_WC is up-to-date
//     * container_WC of thisNode's children are up-to-date
//
// These functions can be useful (from bbox.h):
//
// SetVoidBBox(BBox *A)
//
//    Set A to be the void (empty) BBox
//
// TransformBBox(BBox *A, BBox *B, trfm3D *T);
//
//    Transform B by applying transformation T. Leave result in A.
//
// BoxBox(BBox *A, BBox *B)
//
//    Change BBox A so it also includes BBox B
//

static void nodeUpdateBB (Node *thisNode) {

	Node *oneNode;

	if (thisNode == NULL) return;

	// Get initial BBOX and set to container_WC
	//   if gObject, use his local BBox transformed to WC
	//   else get BBox of first child
	if (thisNode->gObject) {
		TransformBBox (thisNode->container_WC,
					   thisNode->gObject->container,
					   thisNode->placement_WC);
	} else {
		SetVoidBBox(thisNode->container_WC);
		// Increase container_WC with child's BBoxes
		oneNode = StartLoop(thisNode->nodeChilds);
		while(oneNode) {
			BoxBox(thisNode->container_WC,
				   oneNode->container_WC);
			oneNode = GetNext(thisNode->nodeChilds);
		}
	}
}

// Update WC (world coordinates matrix) of a node and
// its bounding box recursively updating all children.
//
// given a node:
//  - update the world transformation (placement_WC) using the parent's WC transformation
//  - recursive call to update WC of the children
//  - update Bounding Box to world coordinates
//
// Precondition:
//
//  - placement_WC of thisNode->parent is up-to-date (or thisNode->parent == NULL)
//

static void nodeUpdateWC( Node *thisNode) {

	Node *oneNode;

	if (thisNode->parent == NULL) {
		SetCopyTrfm3D(thisNode->placement_WC, thisNode->placement); // placement already in world coordinates.
	} else {
		// Compose placement with parent's placement_WC
		SetCopyTrfm3D( thisNode->placement_WC, thisNode->parent->placement_WC);
		CompositionTrfm3D (thisNode->placement_WC, thisNode->placement);
	}

	// update child transformations
	oneNode = StartLoop(thisNode->nodeChilds);
	while(oneNode) {
		nodeUpdateWC(oneNode); // recursive call
		oneNode = GetNext(thisNode->nodeChilds);
	}
	// Now update Bounding Box to world coordinates
	nodeUpdateBB(thisNode);
}

// Auxiliary function
//
// given a node:
// - Update WC of sub-tree starting at node (nodeUpdateWC)
// - Propagate Bounding Box to root (nodePropagateBBRoot)

static void nodeUpdateGS( Node *thisNode) {

	if (thisNode == NULL) return;
	nodeUpdateWC(thisNode);
	nodePropagateBBRoot(thisNode->parent);
}

//
// Attach (group) a Node object into another Node
// Print a warning (and do nothing) if the node has an gObject.

void AttachNode(Node *theNode, Node *theChild) {

	if (theNode == NULL || theChild == NULL) return;
	if (theNode->gObject) {
		fprintf(stderr, "[W] AttachNode: can not attach childs to a node with a gObject.\n");
		return;
	}
	AddLast(theNode->nodeChilds, theChild);
	theChild->parent = theNode;
	// Update WC of the child
	nodeUpdateGS(theChild);
}

//
// Detach a Node object from a Node
//

void DetachNode(Node *theNode) {

	Node *theParent;

	if (theNode == NULL) return;
	theParent = theNode->parent;
	if (theParent == NULL) return; // already detached (or root node)
	theNode->parent = NULL;
	RemoveFromList(theParent->nodeChilds, theNode);
	// Update bounding box of parent
	nodePropagateBBRoot(theParent);
}

//
// Detach all children object from a Node
//

void AllChildsDetachNode(Node *theNode) {

	Node *oneNode;

	if (theNode == NULL) return;
	oneNode = StartLoop(theNode->nodeChilds);
	while(oneNode) {
		oneNode->parent = NULL;
		oneNode = GetNext(theNode->nodeChilds);
	}

	EmptyList(theNode->nodeChilds);
	nodePropagateBBRoot(theNode);
}

void SetGobjNode( Node *thisNode, GObject *gobj ) {
	if (thisNode == NULL) return;
	if (ElementsInList(thisNode->nodeChilds)) {
		fprintf(stderr, "[W] SetGobjNode: can not attach a gObject to a node with childs.\n");
		return;
	}
	thisNode->gObject = gobj;
	nodePropagateBBRoot(thisNode);
}

void SetTrfmNode( Node *thisNode, const trfm3D * M) {
	if (thisNode == NULL) return;
	SetCopyTrfm3D(thisNode->placement, M);
	// Update Geometric state
	nodeUpdateGS(thisNode);
}

void ApplyTrfmNode( Node *thisNode, const trfm3D * M) {
	if (thisNode == NULL) return;
	CompositionTrfm3D(thisNode->placement, M);
	// Update Geometric state
	nodeUpdateGS(thisNode);
}

void TransNode( Node *thisNode, float x, float y, float z ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetTransTrfm3D(&localT, x, y, z);
	ApplyTrfmNode(thisNode, &localT);
};

void RotateXNode( Node *thisNode, float angle ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetRot_X_Trfm3D(&localT, angle);
	ApplyTrfmNode(thisNode, &localT);
};

void RotateYNode( Node *thisNode, float angle ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetRot_Y_Trfm3D(&localT, angle);
	ApplyTrfmNode(thisNode, &localT);
};

void RotateZNode( Node *thisNode, float angle ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetRot_Z_Trfm3D(&localT, angle);
	ApplyTrfmNode(thisNode, &localT);
};

void ScaleNode( Node *thisNode, float factor ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetScaleTrfm3D(&localT, factor);
	ApplyTrfmNode(thisNode, &localT);
};

void AttachLightNode(Node *theNode, Light *theLight) {
	if (theNode == NULL) return;
	theNode->theLight = theLight;
}

void DetachLightNode(Node *theNode) {
	if (theNode == NULL) return;
	theNode->theLight = NULL;
}

void SetShaderNode(Node *theNode, ShaderProgram *theShader) {
	if (theNode == NULL) return;
	theNode->theShader = theShader;
}

void UnsetShaderNode(Node *theNode) {
	if (theNode == NULL) return;
	theNode->theShader = NULL;
}

// Draw a (sub)tree.
//     - First spatial Objects
//     - Then children (depth-first)
//
// These functions can be useful:
//
// rs = RenderStateScene();
//
// MultTrfmRS(rs, MG_MODELVIEW, T); // Apply T transformation
// PushRS(rs, MG_MODELVIEW); // push current matrix into stack
// PushRS(rs, MG_MODELVIEW); // pop matrix from stack to current
//
// DrawGObject(gobj); // draw geometry object
//

void DrawNode(Node *theNode) {

	Node * oneNode;
	ShaderProgram *prev_shader;
	RenderState *rs;
	int i=0;

	if (theNode == NULL) return;
	rs = RenderStateScene();

	if (theNode->isCulled) return;

	// Set shader (save previous)
	if (theNode->theShader != NULL) {
		prev_shader = GetShaderProgramRS(rs);
		SetShaderProgramRS(rs, theNode->theShader);
	}

	// Print BBoxes
	if(rs->drawBBox || theNode->drawBBox)
		DrawGLBBox( theNode->container_WC );

	// Draw geometry object
	if (theNode->gObject) {
		// the world transformation is already precalculated, so just put the
		// transformation in the openGL matrix
		//
		PushRS(rs, MG_MODELVIEW);
		MultTrfmRS(rs, MG_MODELVIEW, theNode->placement_WC);
        // Set model matrix
        LoadTrfmRS(rs, MG_MODEL, theNode->placement_WC);
		DrawGObject( theNode->gObject );
		PopRS(rs, MG_MODELVIEW);
	}

	// draw sub-nodes (recursive)
	oneNode = StartLoop(theNode->nodeChilds);
	while(oneNode) {
		++i;
		DrawNode(oneNode); // Recursive call
		oneNode = GetNext(theNode->nodeChilds);
	}

	if (theNode->theShader != NULL) {
		// restore shader
		SetShaderProgramRS(rs, prev_shader);
	}
}

// @@ TODO: Frustum culling. See if a subtree is culled by the camera, and
//          update isCulled accordingly.

void UpdateCullNode(Node *theNode,
					Camera * cam) {
			Node * oneNode;

			if (theNode == NULL) return;

			if(FrustumCull(cam,theNode->container_WC,0) <= 0)
				theNode->isCulled = 0;
			else
				theNode->isCulled = 1;

			oneNode = StartLoop(theNode->nodeChilds);
			while(oneNode) {
				UpdateCullNode(oneNode,cam); // Recursive call
				oneNode = GetNext(theNode->nodeChilds);
				}
}

// @@ TODO: Check whether a BSphere (in world coordinates) intersects with a
// (sub)tree.
//
// Return a pointer to the Node which collides with the BSphere. NULL
// if not collision.

Node *CollisionBSphereNode(Node *thisNode, BSphere *bsph) {

	int res = 0;
	Node *oneNode, *aux;

	if(thisNode->gObject){
		if(BSphereBBoxIntersect(bsph, thisNode->container_WC ) == IINTERSECT)
			return thisNode;
	}

	oneNode = StartLoop(thisNode->nodeChilds);
	while(oneNode) {
		if(BSphereBBoxIntersect(bsph, oneNode->container_WC)==IINTERSECT){
					return CollisionBSphereNode(oneNode, bsph);
				}
				oneNode = GetNext(thisNode->nodeChilds);
	}

	return NULL; /* No collision */
}
