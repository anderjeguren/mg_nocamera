#version 120

// Bump mapping with many lights.
//
// All computations are performed in the tangent space; therefore, we need to
// convert all light (and spot) directions and view directions to tangent space
// and pass them the fragment shader.

varying vec2 f_texCoord;
varying vec3 f_viewDirection;     // tangent space
varying vec3 f_lightDirection[4]; // tangent space
varying vec3 f_spotDirection[4];  // tangent space

// all attributes in model (object) space
attribute vec3 v_position;
attribute vec3 v_normal; // not really used (use v_TBN_n instead)
attribute vec2 v_texCoord;
attribute vec3 v_TBN_t;
attribute vec3 v_TBN_b;
attribute vec3 v_TBN_n;

uniform mat4 modelToCameraMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToClipMatrix;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)

uniform struct light_t {
  	vec4 position;    // Camera space
  	vec3 ambient;     // rgb
  	vec3 diffuse;     // rgb
  	vec3 specular;    // rgb
  	vec3 attenuation; // (constant, lineal, quadratic)
  	vec3 spotDir;     // Camera space
  	float cosCutOff;  // cutOff cosine
  	float exponent;
} theLights[4];     // MG_MAX_LIGHTS

void main() {
	vec3 L;

	mat3 MV3x3 = mat3(modelToCameraMatrix); // 3x3 modelview matrix

  // Normal, tangent and bitangent in camera coordinates
  vec3 T = normalize(MV3x3 * v_TBN_t);
	vec3 B = normalize(MV3x3 * v_TBN_b);
	vec3 N = normalize(MV3x3 * v_TBN_n);
  //Matrix to transform from camera space to tangent space
  mat3 cameraToTangent = mat3(
      T.x, B.x, N.x,
      T.y, B.y, N.y,
      T.z, B.z, N.z
    );

  vec4 position_camera_space = modelToCameraMatrix * vec4(v_position, 1);
  vec3 viewDirection = normalize(vec3(0, 0, 0) - position_camera_space.xyz);

  f_texCoord = v_texCoord;
  f_viewDirection = normalize(cameraToTangent * viewDirection);
  for(int i=0; i < active_lights_n; ++i){
    if(theLights[i].position.w == 0.0) { /* Direction light */

			L = normalize(-theLights[i].position.xyz);
			L = normalize(cameraToTangent * L);
			f_lightDirection[i] = L;

	 	} else {

	 		vec3 L = normalize(theLights[i].position.xyz - position_camera_space.xyz);
	 		L = normalize(cameraToTangent * L);
	 		f_lightDirection[i] = L;

	 		if(theLights[i].cosCutOff != 0.0){
	 			vec3 spotDirection = normalize(cameraToTangent * normalize(theLights[i].spotDir));
	 			f_spotDirection[i] = normalize(spotDirection);
	 		}
	 	}
  }
  gl_Position = modelToClipMatrix * vec4(v_position, 1.0);
}
