#version 120

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)


uniform struct light_t {
  vec4 position;    // Camera space
  vec3 ambient;     // rgb
  vec3 diffuse;     // rgb
  vec3 specular;    // rgb
  vec3 attenuation; // (constant, lineal, quadratic)
  vec3 spotDir;     // Camera space
  float cosCutOff;  // cutOff cosine
  float exponent;
} theLights[4];     // MG_MAX_LIGHTS

uniform struct material_t {
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float alpha;
  float shininess;
} theMaterial;

attribute vec3 v_position; // Model space
attribute vec3 v_normal;   // Model space
attribute vec2 v_texCoord;

varying vec4 f_color;
varying vec2 f_texCoord;

float diffuse_channel(const vec3 n,
					  const vec3 l) {
	return max(0, dot(n,l));
}

float specular_channel(const vec3 n,
					   const vec3 l,
					   const vec3 v,
					   float m) {
    vec3 r = 2*(max(0.0, dot(n, l)))*n - l;
    return pow(max(0.0, dot(normalize(r), v)), m);
}

void direction_light(const in int i,
					 const in vec3 lightDirection,
					 const in vec3 viewDirection,
					 const in vec3 normal,
					 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float dif = diffuse_channel(normal, lightDirection);
  float spec = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  ambient += theLights[i].ambient * theMaterial.ambient;
	diffuse += dif * theLights[i].diffuse * theMaterial.diffuse;
	specular += spec * theLights[i].specular * theMaterial.specular;
}

void point_light(const in int i,
				 const in vec3 lightDirection,
				 const in vec3 viewDirection,
				 const in vec3 normal,
				 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float dif = diffuse_channel(normal, lightDirection);
  float spec = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

	float dist = distance(theLights[i].position.xyz, v_position);
	float ahuldura = 1.0/((theLights[i].attenuation[0] + theLights[i].attenuation[1]*dist) + (theLights[i].attenuation[2]*pow(dist, 2)));

	ambient += theLights[i].ambient * theMaterial.ambient;
	diffuse += dif * theLights[i].diffuse * theMaterial.diffuse * ahuldura;
	specular += spec * theLights[i].specular * theMaterial.specular * ahuldura;

}

void spot_light(const in int i,
				const in vec3 lightDirection,
				const in vec3 viewDirection,
				const in vec3 normal,
				inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float dif = diffuse_channel(normal, lightDirection);
  float spec = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  float dist = distance(theLights[i].position.xyz, v_position);
	float ahuldura = 1.0/((theLights[i].attenuation[0] + theLights[i].attenuation[1]*dist) + (theLights[i].attenuation[2]*pow(dist, 2)));

	float cspot = max(dot(-lightDirection, theLights[i].spotDir), 0.0);
	if(cspot >= theLights[i].cosCutOff){
		cspot = pow(cspot, theLights[i].exponent);
	}else{
		cspot = 0.0;
	}

	ambient += theLights[i].ambient * theMaterial.ambient;
	diffuse += dif * theLights[i].diffuse * theMaterial.diffuse * cspot * ahuldura;
	specular += spec * theLights[i].specular * theMaterial.specular * cspot * ahuldura;

}

void main() {

	vec3 ambient, diffuse, specular;
	ambient = vec3(0.0);
	diffuse = vec3(0.0);
	specular = vec3(0.0);

	vec4 v_position4 = vec4(v_position.x, v_position.y, v_position.z, 1.0);
	vec4 v_position4C = modelToCameraMatrix * v_position4;

	vec3 viewDirection = normalize(vec3(0.0) - (modelToCameraMatrix * v_position4).xyz);

	vec4 v_normal4 = vec4(v_normal.x, v_normal.y, v_normal.z, 0.0);
	vec3 normal = normalize((modelToCameraMatrix * v_normal4).xyz);

	for(int i=0; i < active_lights_n; ++i) {

		if(theLights[i].position.w == 0.0) { /* Direction light */
		    direction_light(i, normalize(-theLights[i].position.xyz), viewDirection, normal, ambient, diffuse, specular);
	 	} else {
	 		if (theLights[i].cosCutOff == 0.0) { /* Point light */
	 			   point_light(i, normalize(theLights[i].position.xyz - v_position4C.xyz), viewDirection, normal, ambient, diffuse, specular);
	 	  	} else {
	 		     spot_light(i, normalize(theLights[i].position.xyz - v_position4C.xyz), viewDirection, normal, ambient, diffuse, specular);
	 	  	}
	 	}
	}

	f_color = vec4(1, 1, 1, 1);
  gl_Position = modelToClipMatrix * vec4(v_position, 1);
	f_color.rgb = ambient + diffuse + specular;
	f_texCoord = v_texCoord;
}
