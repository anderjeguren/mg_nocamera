#version 120

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)

uniform struct light_t {
	vec4 position;    // Camera space
	vec3 ambient;     // rgb
	vec3 diffuse;     // rgb
	vec3 specular;    // rgb
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
} theLights[4];     // MG_MAX_LIGHTS

uniform struct material_t {
	vec3  ambient;
	vec3  diffuse;
	vec3  specular;
	float alpha;
	float shininess;
} theMaterial;

uniform sampler2D texture0;

varying vec3 f_positionEye;
varying vec3 f_viewDirection;
varying vec3 f_normal;
varying vec2 f_texCoord;

float diffuse_channel(const vec3 n,
					  const vec3 l) {
	return max(0, dot(n,l));
}

float specular_channel(const vec3 n,
					   const vec3 l,
					   const vec3 v,
					   float m) {

						vec3 r = normalize(2*dot(n,l)*n - l);
					  return pow(max(0, dot(r,v)), m);
}

void direction_light(const in int i,
					 const in vec3 lightDirection,
					 const in vec3 viewDirection,
					 const in vec3 normal,
					 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

						 float dif = diffuse_channel(normal, lightDirection);
		 				 float spec = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

		 				 ambient += theLights[i].ambient * theMaterial.ambient;
		 				 diffuse += dif * theLights[i].diffuse * theMaterial.diffuse;
		 				 specular += spec * theLights[i].specular * theMaterial.specular;
}

void point_light(const in int i,
				 const in vec3 lightDirection,
				 const in vec3 viewDirection,
				 const in vec3 normal,
				 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

					float dif = diffuse_channel(normal, lightDirection);
		 		  float spec = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

		 			ambient += theLights[i].ambient * theMaterial.ambient;
		 			diffuse += dif * theLights[i].diffuse * theMaterial.diffuse;
		 			specular += spec * theLights[i].specular * theMaterial.specular;
}

void spot_light(const in int i,
				const in vec3 lightDirection,
				const in vec3 viewDirection,
				const in vec3 normal,
				inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

					float dif = diffuse_channel(normal, lightDirection);
				  float spec = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

					float cspot = max(dot(-lightDirection, theLights[i].spotDir), 0.0);
					if(cspot >= theLights[i].cosCutOff){
						cspot = pow(cspot, theLights[i].exponent);
					}else{
						cspot = 0.0;
					}

					ambient += theLights[i].ambient * theMaterial.ambient;
					diffuse += dif * theLights[i].diffuse * theMaterial.diffuse * cspot;
					specular += spec * theLights[i].specular * theMaterial.specular * cspot;
}

void main() {
	vec4 position_camera_space;
  vec3 viewDirection;

  vec3 ambient = vec3(0.0);
  vec3 diffuse = vec3(0.0);
  vec3 specular = vec3(0.0);

  vec3 normal = f_normal;

  float spotAngle;
  float cspot;

  position_camera_space = vec4(f_positionEye,1);
  viewDirection = f_viewDirection;

  for(int i=0; i < active_lights_n; ++i) {
	if(theLights[i].position.w == 0.0) {
	  // direction light
	  direction_light(i,
	  				  -normalize(vec3(theLights[i].position)), // lightDirection
	  				  viewDirection,
	  				  normal,
	  				  ambient, diffuse, specular);
	} else {
	  if (theLights[i].cosCutOff == 0.0) {
		point_light(i,
					normalize(vec3(theLights[i].position - position_camera_space)),
					viewDirection,
					normal,
					ambient, diffuse, specular);
	  } else {
		spot_light(i,
				   normalize(vec3(theLights[i].position - position_camera_space)),
				   viewDirection,
				   normal,
				   ambient, diffuse, specular);
	  }
	}
  }
	vec4 f_color = vec4(1);
  f_color.rgb = ambient  + diffuse + specular;
	vec4 color = texture2D(texture0,f_texCoord);
	gl_FragColor = color  * f_color;
}
