#version 120

attribute vec3 v_position;
attribute vec3 v_normal;
attribute vec2 v_texCoord;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

varying vec3 f_positionEye;
varying vec3 f_viewDirection;
varying vec3 f_normal;
varying vec2 f_texCoord;

void main() {

  vec4 position_camera_space = modelToCameraMatrix * vec4(v_position, 1.0);

  f_positionEye = vec3(position_camera_space);
  f_viewDirection = -normalize(vec3(position_camera_space));
	f_normal = normalize(vec3(modelToCameraMatrix * vec4(v_normal, 0.0)));
	f_texCoord = v_texCoord;

	gl_Position = modelToClipMatrix * vec4(v_position, 1.0);

}
